import math
import random
from collections import deque

import networkx as nx


class MazeAlgorithm:

    def generate(self, maze):
        pass


# note: using visitor pattern for generating mazes.
class RecursiveBackTracker(MazeAlgorithm):

    def generate(self, maze):
        stack = deque()


class CompletelyRandomGenerator(MazeAlgorithm):
    def generate(self, maze):
        G = maze.G

        # randomly assign edges in graph
        for node in G.nodes():
            choice = random.choice([[-1, 0], [1, 0], [0, 1], [0, -1], [0, 0]])

            if node[0] == 0 and choice[0] == -1:
                choice[0] = random.randint(0, 1)
            elif node[0] == 10 and choice[0] == 1:
                choice[0] = random.randint(-1, 0)

            if node[1] == 0 and choice[1] == -1:
                choice[1] = random.randint(0, 1)
            elif node[1] == 20 and choice[1] == 1:
                choice[1] = random.randint(-1, 0)

            if (choice[0], choice[1]) in ((1, 1), (-1, -1), (-1, 1), (1, -1)):
                if random.randint(0, 1) > 0:
                    choice[0] = 0
                else:
                    choice[1] = 0

            if (0, 0) != (choice[0], choice[1]):
                G.add_edge(node, (node[0] + choice[0], node[1] + choice[1]))


class TruePrimsMST(MazeAlgorithm):
    def __init__(self, starting_node=(0, 0)):
        self.starting_node = starting_node

    def generate(self, maze):

        for i in maze.G.edges():
            maze.G[i[0]][i[1]]['weight'] = random.randint(1, 10)

        edge = {}
        cost = {}
        for node in maze.G.nodes():
            cost[node] = math.inf
            edge[node] = None

        cost[self.starting_node] = 0
        TEMP = list(maze.G.nodes()).copy()
        T = []
        while len(TEMP) != 0:
            v = min(cost, key=cost.get)
            TEMP.remove(v)
            if edge[v] is not None:
                T.append(edge[v])
            for neighbor in maze.G.neighbors(v):
                if neighbor in TEMP and cost[neighbor] > maze.G[v][neighbor]["weight"]:
                    cost[neighbor] = maze.G[v][neighbor]["weight"]
                    edge[neighbor] = (v, neighbor)

            del cost[v]
            # del edge[v]

        maze.G = nx.Graph(T)
