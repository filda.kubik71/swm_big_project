import pickle

import networkx as nx


# import matplotlib.pyplot as plt


class Maze:

    # is_empty is an optional argument, which defines whether we want a maze that:
    # has no passages or a maze that has no walls.
    def __init__(self, n_rows=5, n_columns=5, is_empty=True):
        # G = None
        if is_empty:
            G = nx.empty_graph([(y, x) for x in range(n_rows) for y in range(n_columns)])
        else:
            G = nx.grid_2d_graph(n_columns, n_rows)

        self.G = G
        self.n_rows = n_rows
        self.n_columns = n_columns
        self.empty_start = is_empty

    def draw_maze(self, offset=None):
        # display position with inverted Y ( [0,0] is in top left corner of graph, [n-1,n-1] in the right bottom )
        if offset is None:
            offset = [0, 0]
        pos = {node: (node[0] + offset[0], -node[1] + offset[1]) for node in self.G.nodes()}
        nx.draw(self.G, pos=pos, with_labels=False, node_size=10)

    def generate(self, maze_algorithm_type):
        maze_algorithm_type.generate(self)

    def save(self, filename):
        with open(filename, "wb") as output:
            pickle.dump(self, output, pickle.HIGHEST_PROTOCOL)

    def load(self, filename):
        with open(filename, "rb") as _input:
            loaded = pickle.load(_input)

        return loaded

    # def loads(self, filename):
    #     with open(filename, "rb") as _input:
    #         self = pickle.load(_input)


def load_maze(filename):
    with open(filename, "rb") as _input:
        loaded = pickle.load(_input)

    return loaded
