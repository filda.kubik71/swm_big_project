import yaml


class _SettingsLoader:
    def __init__(self, filename):
        self.settings = None
        self.base_settings = """
        game_settings:
          tile_size: 15
          
        
        """

        try:
            with open(filename, encoding="utf-8") as file:
                self.settings = file.read()

            self.settings: dict = yaml.safe_load(self.settings)
            self.game_settings = self.settings["game_settings"]
        except FileNotFoundError:
            print("No settings file found, creating one from scratch")
            with open(filename, "w+", encoding="utf-8") as file:
                yaml.dump(self.base_settings, file)

    def get_setting(self, root_tree, setting_name):
        return self.settings[root_tree][setting_name]


settings = _SettingsLoader("settings.yaml")
