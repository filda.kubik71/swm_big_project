# Maze generators

The maze object can generate mazes depending on given generators.

## Generators

- how a maze is generated
- are carving/building
- maze object itself does not know, how the generator operates,
therefore generators can have special parameters

**Refer to [this page](/basic_generators/)
for more information about the built-in generators.**
