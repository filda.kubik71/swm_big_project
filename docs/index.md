# About the project

PyGameMaze is a project meant for creating mazes - going through those mazes
and, mainly, performing analysis of how the player does
while trying to solve a maze.

## Project roadmap

- [x] Creation of mazes using graphs
  - [x] Saving and loading maze objects for later use
- [x] A "Game" where the player can go through a maze
- [ ] analysis of player performance
