# Prism's Minimal Spanning Tree generator

This generator is a **carver** with time complexity of $O(n^{2})$.

## How it works

1. Initiate by declaring which node is the starting one;

    ```py
    def __init__(self, starting_node=(0, 0)):
        self.starting_node = starting_node
    ```

1. Put random weights in all of the paths in the graph;

    ```py
    for i in maze.G.edges():
        maze.G[i[0]][i[1]]['weight'] = random.randint(1, 10)
    ```

1. Make lists for the costs and edges and initialize them as such;

    ```py
    edge = {}
    cost = {}
    for node in maze.G.nodes():
        cost[node] = math.inf
        edge[node] = None

    cost[self.starting_node] = 0
    ```

1. Copy the graph's nodes into a new (temporary) one and make a new list of edges;

    ```py
    TEMP = list(maze.G.nodes()).copy()
    T = []
    ```

1. While there are nodes in the temporary graph:

    1. get the one with the *lowest* cost and remove it from the temporary list;

        ```py
        v = min(cost, key=cost.get)
        TEMP.remove(v)
        ```

    1. if the edge is in the list of edges (`edges`), then we append it to `T`;

        ```py
        if edge[v] is not None:
            T.append(edge[v])
        ```

    1. go throught all the neighbors of `v` and if the cost to get to the
    neighbor (`cost[neighbor]`)
    is higher than that of the edge connecting it,
    then write the edges' cost to `cost[neighbor]` and add the edge to `edges`;

        ```py
        for neighbor in maze.G.neighbors(v):
            if neighbor in TEMP and cost[neighbor] > maze.G[v][neighbor]["weight"]:
                cost[neighbor] = maze.G[v][neighbor]["weight"]
                edge[neighbor] = (v, neighbor)
        ```

    1. remove the current node `v` from `cost` list;

        ```py
        del cost[v]
        ```

1. Finally, return a new graph created from our temporary edges `T`;

    ```py
    # set the maze's graph to the return of Prisms' MST algorithm
    maze.G = nx.Graph(T)
    ```

## All together, here is the full code

```py
class TruePrimsMST(MazeAlgorithm):
    def __init__(self, starting_node=(0, 0)):
        self.starting_node = starting_node

    def generate(self, maze):

        for i in maze.G.edges():
            maze.G[i[0]][i[1]]['weight'] = random.randint(1, 10)

        edge = {}
        cost = {}
        for node in maze.G.nodes():
            cost[node] = math.inf
            edge[node] = None

        cost[self.starting_node] = 0
        TEMP = list(maze.G.nodes()).copy()
        T = []
        while len(TEMP) != 0:
            v = min(cost, key=cost.get)
            TEMP.remove(v)
            if edge[v] is not None:
                T.append(edge[v])
            for neighbor in maze.G.neighbors(v):
                if neighbor in TEMP and cost[neighbor] > maze.G[v][neighbor]["weight"]:
                    cost[neighbor] = maze.G[v][neighbor]["weight"]
                    edge[neighbor] = (v, neighbor)

            del cost[v]
            # del edge[v]

        maze.G = nx.Graph(T)

```
