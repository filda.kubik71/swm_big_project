import argparse
import os

from Game.app import App
from Model.maze import load_maze

from Game.mazecreator import MazeCreator


def get_arg_parser():
    _arg_parser = argparse.ArgumentParser()

    _arg_parser.add_argument(
        "input_file",
        nargs="?",
        help="Input file path",
        default=os.path.join(os.getcwd(), "output"),
    )

    return _arg_parser


if __name__ == '__main__':
    arg_parser = get_arg_parser()
    args = arg_parser.parse_args()

    maze = load_maze(args.input_file)
    app = App()

    creator = MazeCreator(maze.G)
    app.initialize(creator.blocks)
    app.run()

    # maze.draw_maze()
    # plt.show()
