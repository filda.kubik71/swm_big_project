# SSP Final Project

[Pages link](https://filda.kubik71.gitlab.io/swm_big_project/)

## Installation
```
pip install -r requirements.txt
```

## Usage

### Generate maze

```
python generate.py output_file n_rows n_cols
```
if n_rows or n_cols arguments aren't specified, then they default to 20.

### Load maze and start solving

```
python main.py input_file
```

## Hodnotící kritéria (CZ)

Požadavky na kvalitu práce:

- [x] GIT repozitář na Gitlabu.
  - [x] Repozitář má README.md (K čemu projekt slouží, jak se používá, jak se instaluje, jaké jsou prerekvizity,
    apod.)
- [x] Použití vlastního projektu (ideální kandidát má kombinaci jazyků - např. Python a BASH, Markdown a JS, apod.)
- [x] Vytvořená CI v repozitáři.
- [x] CI má minimálně tři úlohy:
  - [x] Test kvality Markdown stránek. (Kvalita dokumentace je důležitá. Minimálně README.md by mělo být v repozitáři
    dostupné.)
  - [x] Kontrola syntaxe každého jazyka.
  - [x] Použití "lint" nástroje pro každý jazyk.

