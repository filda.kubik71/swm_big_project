import argparse
import os

from matplotlib import pyplot as plt

from Model.maze import Maze
from Model.maze_generators import TruePrimsMST


def get_arg_parser():
    _arg_parser = argparse.ArgumentParser()

    _arg_parser.add_argument(
        "input_file",
        nargs="?",
        help="Input file path",
        default=os.path.join(os.getcwd(), "output"),
    )
    _arg_parser.add_argument(
        "n_rows",
        nargs="?",
        help="Input number of columns",
        default=20,
    )
    _arg_parser.add_argument(
        "n_cols",
        nargs="?",
        help="Input number of rows",
        default=20,
    )

    return _arg_parser


if __name__ == '__main__':
    arg_parser = get_arg_parser()
    args = arg_parser.parse_args()

    filepath = args.input_file
    n_rows = args.n_rows
    n_cols = args.n_cols

    maze = Maze(n_rows, n_cols, False)
    maze.generate(TruePrimsMST())

    maze.draw_maze()
    plt.show()

    maze.save("test_output")
