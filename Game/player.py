import pygame
from Game.drawable import Drawable
from Game.collidable import Collidable
from Game.block import Block


class Player(Drawable, Collidable):
    def __init__(self, x=0, y=0, width=10, height=10, speed=0.8):
        super().__init__(x, y, width, height)
        self.movement_x = 0
        self.movement_y = 0
        self.speed = speed

    def handle_keys(self, event):
        if event.type == pygame.KEYDOWN:
            match event.key:
                case pygame.K_UP:
                    self.movement_y = -1
                case pygame.K_DOWN:
                    self.movement_y = 1
                case pygame.K_RIGHT:
                    self.movement_x = 1
                case pygame.K_LEFT:
                    self.movement_x = -1
        elif event.type == pygame.KEYUP:
            match event.key:
                case pygame.K_UP:
                    self.movement_y = 0
                case pygame.K_DOWN:
                    self.movement_y = 0
                case pygame.K_RIGHT:
                    self.movement_x = 0
                case pygame.K_LEFT:
                    self.movement_x = 0

    def update(self, window, world=None):
        colliding_with = []
        # get all Blocks colliding with the player into a list
        for entity in world.entities:
            if entity is not self:
                if isinstance(entity, Block):
                    if entity.is_colliding(self.get_collide_rect().move(self.movement_x * self.speed, self.movement_y * self.speed)):
                        colliding_with.append(entity)
                        entity.color = (0, 0, 255)
                    else:
                        entity.color = (255, 255, 255)

        # if the Block is still in collision after player movement, then don't let the player through
        self.x += self.movement_x * self.speed
        for entity in colliding_with:
            if entity.is_colliding(self.get_collide_rect()):
                if self.movement_x != 0:
                    if self.movement_x < 0:
                        self.x = entity.x + entity.width
                    else:
                        self.x = entity.x - self.width

        self.y += self.movement_y * self.speed
        for entity in colliding_with:
            if entity.is_colliding(self.get_collide_rect()):
                if self.movement_y != 0:
                    if self.movement_y < 0:
                        self.y = entity.y + entity.height
                    else:
                        self.y = entity.y - self.height
