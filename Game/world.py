import pygame.surface
from Game.camera import Camera


class World:
    def __init__(self, window: pygame.surface.Surface):
        self.entities = []
        self.camera = Camera()
        self.camera.width = window.get_width()
        self.camera.height = window.get_height()
        self.window = window

    def add_entity(self, entity):
        self.entities.append(entity)

    def update(self):
        for entity in self.entities:
            if hasattr(entity, "update"):
                entity.update(self.window, self)
        self.camera.update()

    def draw(self):
        for entity in self.entities:
            entity.draw(self.window, self.camera)
