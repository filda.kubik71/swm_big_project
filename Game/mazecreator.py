from Game.block import Block
import settings_loader
tile_size = settings_loader.settings.get_setting("game_settings", "tile_size")

testing_i = 1
colors = [(255, 255, 255), (255, 0, 0), (0, 255, 0), (0, 0, 255)]
color = colors[0]


class MazeCreator:

    def __init__(self, graph):
        self.blocks: list[Block] = []
        for node in graph.nodes:
            # color = colors[i % len(colors)]
            # if i > 10:
            #     break
            edges = graph[node]
            # rlud
            flags = 0b0000
            x = node[0]
            y = node[1]
            for edge in edges:
                if edge[0] == x or edge[1] == y:
                    if edge[0] > x:
                        flags |= 8
                    elif edge[0] < x:
                        flags |= 4
                    if edge[1] > y:
                        flags |= 1
                    elif edge[1] < y:
                        flags |= 2

            room_x = x * 9
            room_y = y * 9

            if not flags & 8:
                self.make_right_wall(room_x, room_y, 10, 10)
            if not flags & 4:
                self.make_left_wall(room_x, room_y, 10)
            if not flags & 1:
                self.make_bottom_wall(room_x, room_y, 10, 10)
            if not flags & 2:
                self.make_top_wall(room_x, room_y, 10)
            # self.make_room(room_x, room_y, 10, 10)

        self.blocks = list(set(self.blocks))

    def make_horizontal_wall(self, x, y, width) -> list[Block]:
        for i in range(width):
            self.blocks.append(Block(x + i, y, color))
        return self.blocks

    def make_vertical_wall(self, x, y, height) -> list[Block]:
        for i in range(height):
            self.blocks.append(Block(x, y + i, color))
        return self.blocks

    def make_top_wall(self, x, y, width):
        self.make_horizontal_wall(x, y, width)

    def make_bottom_wall(self, x, y, height, width):
        self.make_horizontal_wall(x, y + height - 1, width)

    def make_left_wall(self, x, y, height):
        self.make_vertical_wall(x, y + 1, height - 1)

    def make_right_wall(self, x, y, width, height):
        self.make_vertical_wall(x + width - 1, y + 1, height - 1)

    def make_room(self, x, y, width, height):
        self.make_horizontal_wall(x, y, width)
        self.make_horizontal_wall(x, y + height - 1, width)
        self.make_vertical_wall(x, y + 1, height - 1)
        self.make_vertical_wall(x + width - 1, y + 1, height - 1)
        return self.blocks

    def get_blocks(self):
        return self.blocks
