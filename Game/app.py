import pygame
from pygame import init, display

from Game.world import World
from Game.player import Player
from Game.keyhandler import KeyHandler
from settings_loader import settings

init()
clock = pygame.time.Clock()

font_type = settings.get_setting("other_settings", "font_type")
font_size = settings.get_setting("other_settings", "font_size")
font = pygame.font.Font('freesansbold.ttf', font_size)


class App:

    def __init__(self):

        self.window = display.set_mode((980, 480))

        self.plr = Player(x=30, y=30, speed=4)

        self.use_zoom = True

        self.delta_t = 0

        self.world = World(self.window)

        # blocks = LevelParser("Game/world.txt").get_level()

        self.world.add_entity(self.plr)
        camera = self.world.camera
        camera.target = self.plr

        self.running = True
        self.key_handler = KeyHandler(self)

    def initialize(self, blocks):
        for block in blocks:
            self.world.add_entity(block)

    def run(self):
        while self.running:

            event_list = pygame.event.get()
            for event in event_list:
                if event.type == pygame.QUIT:
                    self.running = False

                self.key_handler.handle_keys(event)
                self.world.camera.handle_keys(event)

                if event.type in (pygame.KEYDOWN, pygame.KEYUP):
                    self.plr.handle_keys(event)

            self.key_handler.update()
            self.world.update()

            self.window.fill((0, 0, 0))

            self.world.draw()
            to_draw_text = f"fps = {clock.get_fps()}\n#entities = {len(self.world.entities)}"
            for y, line in enumerate(to_draw_text.splitlines()):
                text = font.render(line, True, (255, 255, 255))
                text_x = 0
                text_y = 0 + y * text.get_size()[1]
                self.window.blit(text, (text_x, text_y))

            display.update()
            clock.tick(60)

        pygame.quit()
        pygame.font.quit()
