import pygame


class Drawable:
    def __init__(self, x, y, width, height, texture=None):
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        # use texture in the future?
        self.texture = texture
        if texture is None:
            self.color = (0, 0, 255)
            self.rect = pygame.Rect(x, y, width, height)

    def draw(self, screen, camera):
        if self.texture is None:
            x = (self.x * camera.zoom) - camera.x
            y = (self.y * camera.zoom) - camera.y
            width = self.width * camera.zoom
            height = self.height * camera.zoom
            # x = (self.rect.x * camera.zoom) - camera.x
            # y = (self.rect.y * camera.zoom) - camera.y
            # width = self.rect.width * camera.zoom
            # height = self.rect.height * camera.zoom

            pygame.draw.rect(screen, self.color, (x, y, width, height))
            return
