from pygame import Rect


class Collidable:
    x = 0
    y = 0
    width = 0
    height = 0

    def get_collide_rect(self) -> Rect:
        if None in (self.x, self.y, self.width, self.height):
            return Rect(0, 0, 0, 0)
        return Rect(self.x, self.y, self.width, self.height)

    def is_colliding(self, other) -> bool:
        if isinstance(other, Collidable):
            return self.get_collide_rect().colliderect(other.get_collide_rect())
        if isinstance(other, Rect):
            return self.get_collide_rect().colliderect(other)
