from Game.drawable import Drawable
from Game.collidable import Collidable
import settings_loader
tile_size = settings_loader.settings.get_setting("game_settings", "tile_size")


class Block(Drawable, Collidable):
    def __init__(self, x, y, color, width=tile_size, height=tile_size):
        super().__init__(x * tile_size, y * tile_size, width, height)
        self.color = color
