import pygame

from Game.keyhandles import KeyHandles


class keyhandles(KeyHandles):
    def handle_ESCAPE(self, value):
        if value:
            # print("test")
            self.context.running = False


class KeyHandler:
    def __init__(self, context, keyhandles_=None):
        if keyhandles_ is None:
            keyhandles_ = keyhandles(context)
        else:
            keyhandles_ = keyhandles_(context)
        self.keyhandles = keyhandles_

        self.callbacks = {}
        self.pressed_keys = {}

        self.add_handlers(*self.keyhandles.mthds)

    def add_handler(self, key, callback):
        if key not in self.callbacks:
            self.callbacks[key] = callback
        else:
            self.callbacks[key].append(callback)

    def add_handlers(self, keys, callbacks):
        for (key, callback) in zip(keys, callbacks):
            self.add_handler(key, callback)

    def handle_keys(self, event):
        if event.type == pygame.KEYDOWN:
            self.pressed_keys[event.key] = True
        elif event.type == pygame.KEYUP:
            self.pressed_keys[event.key] = False

    def update(self):
        for key, value in self.pressed_keys.items():
            if key in self.callbacks:
                if isinstance(self.callbacks[key], list):
                    for callback in self.callbacks[key]:
                        callback(self.keyhandles, value)
                else:
                    self.callbacks[key](self.keyhandles, value)
