from Game.block import Block
from settings_loader import settings

tile_size = settings.get_setting("game_settings", "tile_size")


block_dict = {
    "1": lambda x, y: Block(x, y, (255, 255, 255)),
    "0": lambda x, y: Block(x, y, (0, 0, 0))
}


class LevelParser:
    def __init__(self, filepath):
        self.level = []

        with open(filepath, encoding="utf-8") as file:
            lines = file.readlines()
            for y, line in enumerate(lines):
                for x, letter in enumerate(line):
                    if not letter.isnumeric():
                        continue
                    if letter != "0":
                        self.level.append(block_dict[letter](x, y))

    def get_level(self):
        return self.level
