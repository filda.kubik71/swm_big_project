import pygame


class KeyHandles:
    def __init__(self, context):
        self.context = context
        self.mthds = self.get_stuff(self.__class__)

    def get_stuff(self, _class):
        methods = [(name, func) for name, func in vars(_class).items() if
                   callable(getattr(_class, name)) and not name.startswith("__")]
        keys = [vars(pygame)["K" + (method[0].removeprefix("handle"))] for method in methods]
        methods = [func[1] for func in methods]

        return keys, methods
