from Game.keyhandler import KeyHandler
from Game.keyhandles import KeyHandles


class keyHandles(KeyHandles):
    def handle_d(self, value):
        if value:
            self.context.x += 1

    def handle_a(self, value):
        if value:
            self.context.x -= 1

    def handle_w(self, value):
        if value:
            self.context.y -= 1

    def handle_s(self, value):
        if value:
            self.context.y += 1

    def handle_q(self, value):
        if value:
            self.context.zoom -= .01

    def handle_e(self, value):
        if value:
            self.context.zoom += .01

    def handle_c(self, value):
        if value:
            print(self.context.zoom, self.context.x, self.context.y)


class Camera:
    def __init__(self, target=None):
        self.zoom = 1.
        self.x = 0
        self.y = 0
        self.width = 100
        self.height = 100
        self.target = target
        self.key_handler = KeyHandler(self, keyHandles)

    def update(self):
        if self.zoom < 0.09:
            self.zoom = 0.09

        if self.target is not None:
            self.x = (self.target.x * self.zoom) - self.width / 2
            self.y = (self.target.y * self.zoom) - self.height / 2

        self.key_handler.update()

    def handle_keys(self, event):
        self.key_handler.handle_keys(event)
